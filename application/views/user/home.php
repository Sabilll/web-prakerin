<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta cherset="utf-8">
    
    <title><?= $title; ?> </title>
    
    <!-- Custom styles for this template-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/seacrh.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/perusahaan.css">

</head>

<body >
<nav class="navbar navbar-expand navbar-light bg-transparent topbar mb-4 static-top shadow navbar-position-fixed" style="position: fixed; margin-top: -10%; width: 100%">
            <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                <i class="fa fa-bars"></i>
            </button>


            <h1 class="h3 mb-4 text-gray-800"> Selamat datang <?= $user['name']; ?> !</h1>

            <!-- Topbar Navbar -->
            <ul class="navbar-nav ml-auto">
                <div class="topbar-divider d-none d-sm-block"></div>

                <!-- Nav Item - User Information -->
                <li class="nav-item dropdown no-arrow">
                    <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $user['name']; ?></span>
                        <img class="img-profile rounded-circle" src="<?= base_url('assets/img/profile/') . $user['image'] ?>">

                    </a>
                    <!-- Dropdown - User Information -->
                </li>
                <li>
                    <div>
                        <a class="dropdown-item" href="<?= base_url('auth/logout'); ?>"  data-toggle="modal" data-target="#logoutModal">
                            <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                            Logout
                        </a>
                        <a class="dropdown-item" href="#">
                            <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                            Profile
                        </a>
                    </div>
                </li>


            </ul>

        </nav>

    <div class="seacrh-box">
      <input class="seacrh-txt" type="text" name="keyword" placeholder="Type to Seacrh">
      <a class="seacrh-btn" href="#" name="search_submit">
        <i class="fas fa-search"></i>
      </a>
      <!-- <form action="<?= base_url('user/search'); ?>" method="POST" style="float: right;">
            <input type="text" name="cari" placeholder="Cari disini..">
            <?= form_error('cari'); ?>

            <input type="submit" name="submit" value="cari">
        </form> -->
    </div>
    <div class="perusahaan" style="margin-top:10%; margin-right:15%; margin-left:15%">
        <div class="row">
                  <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                              <?php foreach ($products as $product) : ?>
                                  <div class="hovereffect">
              
                                      <img src="<?php echo base_url('assets/img/product/' . $product->image) ?>"  />
                                      <div class="overlay">
                                          <h2><?php echo $product->name ?></h2>
                                          <a class="info" href="<?php echo site_url('admin/Products/detail/' . $product->product_id) ?>">Read More</a>
                                      </div>
                                  </div>
                              <?php endforeach; ?>
                  </div>
                      <!-- <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                          <div class="hovereffect">
                              <img class="img-responsive" src="<?php echo base_url(); ?>assets/image/Sebo.png" alt="">
                              <div class="overlay">
                                  <h2>Sebo</h2>
                                  <a class="info" href="#">Read More</a>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                          <div class="hovereffect">
                              <img class="img-responsive" src="<?php echo base_url(); ?>assets/image/Arkademy.png" alt="">
                              <div class="overlay">
                                  <h2>Arkademy</h2>
                                  <a class="info" href="#">Read Moreds</a>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                          <div class="hovereffect">
                              <img class="img-responsive" src="<?php echo base_url(); ?>assets/image/Omni.png" alt="">
                              <div class="overlay">
                                  <h2>Omni</h2>
                                  <a class="info" href="#">Read More</a>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                          <div class="hovereffect">
                              <img class="img-responsive" src="<?php echo base_url(); ?>assets/image/Sebo.png" alt="">
                              <div class="overlay">
                                  <h2>Sebo</h2>
                                  <a class="info" href="#">Read More</a>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                          <div class="hovereffect">
                              <img class="img-responsive" src="<?php echo base_url(); ?>assets/image/Arkademy.png" alt="">
                              <div class="overlay">
                                  <h2>Arkademy</h2>
                                  <a class="info" href="#">Read Moreds</a>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                          <div class="hovereffect">
                              <img class="img-responsive" src="<?php echo base_url(); ?>assets/image/Omni.png" alt="">
                              <div class="overlay">
                                  <h2>Omni</h2>
                                  <a class="info" href="#">Read More</a>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                          <div class="hovereffect">
                              <img class="img-responsive" src="<?php echo base_url(); ?>assets/image/Sebo.png" alt="">
                              <div class="overlay">
                                  <h2>Sebo</h2>
                                  <a class="info" href="#">Read More</a>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                          <div class="hovereffect">
                              <img class="img-responsive" src="<?php echo base_url(); ?>assets/image/Arkademy.png" alt="">
                              <div class="overlay">
                                  <h2>Arkademy</h2>
                                  <a class="info" href="#">Read Moreds</a>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                          <div class="hovereffect">
                              <img class="img-responsive" src="<?php echo base_url(); ?>assets/image/Omni.png" alt="">
                              <div class="overlay">
                                  <h2>Omni</h2>
                                  <a class="info" href="#">Read More</a>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                          <div class="hovereffect">
                              <img class="img-responsive" src="<?php echo base_url(); ?>assets/image/Sebo.png" alt="">
                              <div class="overlay">
                                  <h2>Sebo</h2>
                                  <a class="info" href="#">Read More</a>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                          <div class="hovereffect">
                              <img class="img-responsive" src="<?php echo base_url(); ?>assets/image/Arkademy.png" alt="">
                              <div class="overlay">
                                  <h2>Arkademy</h2>
                                  <a class="info" href="#">Read Moreds</a>
                              </div>
                          </div>
                      </div> -->

        </div>

        </div>
        <script>
		function deleteConfirm(url) {
			// $('#btn-delete').attr('href', url);
			$('#deleteModal').modal();
		}
	</script>

</body>
