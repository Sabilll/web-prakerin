<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">


	<div id="wrapper">


		<div id="content-wrapper">

			<div class="container-fluid">
				<!-- <h1 class="h3 mb-4 text-gray-800"> <?= $title; ?></h1> -->

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>

				<!-- DataTables -->
				<div class="card mb-3">
					<!-- <div class="card-header">
						<a href="<?php echo site_url('admin/biodata/add') ?>"><i class="fas fa-plus"></i> Add New</a>
					</div> -->
					<div class="card-body">

						<div class="table-responsive">
							<table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>nisn</th>
										<th>nama</th>
										<th>ttl</th>
										<th>alamat</th>
										<th>kelas</th>
										<th>email</th>
										<th>perusahaan</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($biodata as $biodata) : ?>
										<tr>
											<td width="150">
												<?php echo $biodata->nisn ?>
											</td>
											<td>
												<?php echo $biodata->nama ?>
											</td>
											<td>
												<?php echo $biodata->ttl ?>
											</td>
											<td>
												<?php echo $biodata->alamat ?>
											</td>
											<td>
												<?php echo $biodata->kelas ?>
											</td>
											<td>
												<?php echo $biodata->email ?>
											</td>
											<td>
												<?php echo $biodata->perusahaan ?>
											</td>
						
												<!-- <a href="<?php echo site_url('admin/biodata/edit/' . $biodata->biodata_id) ?>" class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
												<a onclick="deleteConfirm('<?php echo site_url('admin/biodata/delete/' . $biodata->biodata_id) ?>')" href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Hapus</a> -->
											</td>
										</tr>
									<?php endforeach; ?>

								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>
			<!-- /.container-fluid -->

			<!-- Sticky Footer -->

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->


	<?php $this->load->view("admin/_partials/scrolltop.php") ?>
	<?php $this->load->view("admin/_partials/modal.php") ?>

	<?php $this->load->view("admin/_partials/js.php") ?>

	<script>
		function deleteConfirm(url) {
			$('#btn-delete').attr('href', url);
			$('#deleteModal').modal();
		}
	</script>
</body>

</html>