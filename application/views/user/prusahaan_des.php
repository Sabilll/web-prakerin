<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/des.css">
</head>

<body>
    <div class="img-detail">
        <img src="<?php echo base_url(); ?>assets/image/omni1.png">
    </div>
    <div>
        <h2>Omnicreativora</h2>
    </div>
    <div class="deskripsi">
        <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Deskripsi singkat tentang perusahaan atau studio. Menjelaskan latar belakang perusahan agar siswa bisa lebih mengenal perusahaan yang akan dituju. Menjelaskan apa yang akan dilakukan oleh siswa secara umum agar siswa mendapat gambaran saat pkl di perusahan tersebut.<br><br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Menjelaskan apa yang akan didapat siswa disana dan berapa jumlah siswa yang dibutuhkan secara jelas. memberitahukan fasilitas apa saja yang akan didapat di perusahaan tersebut. Dan menjelaskan jam kerja dan peraturan dari masing-masing perusahaan. Serta Melampirkan alamat perusahaan secara detail.</p>
    </div>
    <div class="syarat">

        <p><b>Syarat dan ketentuan</b></p>

    </div>
    <div class="persyartan">
        <ul>
            <li> Masih berstatus siswa SMK aktif ,</li>
            <li> Membawa Data Pribadi (KTP/ Kartu Pelajar) ,</li>
            <li> Membawa Fotocopy Surat Pengajuan Prakerin dari sekolah ,</li>
            <li> Membawa Fotocopy Rapor / Rekap Nilai s.d semester terakhir ,</li>
            <li> Membawa Softcopy Curriculum Vitae (CV), dengan ketentuan format pdf ,</li>
            <li> Membawa 3 pas foto berwarna dan berukuran 2x3 ,</li>
            <li> Membawa Surat Keterangan Baik dari sekolah ,</li>
            <li> Membawa Surat kesehatan dari dokter</li>
        </ul>
        <div class="jurusan">
            <text><b>Jururusan yang diterima</b></text> <br>
            &nbsp;&nbsp; &nbsp;&nbsp;<text>TJA &nbsp;&nbsp; | &nbsp;&nbsp; RPL &nbsp;&nbsp; | &nbsp;&nbsp; TKJ</text>
        </div>
        <div class="btn-daftar">
            <button type="submit">Daftar</button>
        </div>
    </div>
</body>

</html>