<div class="table-responsive">
    <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
        <tbody>
            <?php foreach ($products as $product) : ?>
                <tr>
                    <td width="150">
                        <?php echo $product->name ?>
                    </td>
                    <td>
                        <?php echo $product->price ?>
                    </td>
                    <td>
                        <img src="<?php echo base_url('assets/img/product/' . $product->image) ?>" width="64" />
                    </td>
                    <td class="small">
                        <?php echo substr($product->description, 0, 120) ?>...</td>
                    <td width="250">
                        <a href="<?php echo site_url('user/detail/' . $product->product_id) ?>" class="btn btn-small"><i class="fas fa-edit"></i> Detail</a>
                    </td>
                </tr>
            <?php endforeach; ?>

        </tbody>
    </table>
</div>