<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $title; ?> </title>


    <!-- Custom styles for this template-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/biodata.css">

</head>


<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-lg-7">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <!-- <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> -->
                        <div class="col-lg">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Isi Biodatamu !</h1>
                                </div>
                                <?= $this->session->flashdata('message'); ?>
                                <!-- <form class="user" method="POST" action="<?= base_url('auth'); ?>"> -->
                                <form class="user" method="POST" action="<?= base_url('biodata/Biodata'); ?> ">

                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-user" id="nisn" name="nisn" placeholder="Enter NISN "value="<?= set_value('nisn'); ?>" >
                                        <?= form_error('Nisn', '<small class="text-danger pl-3">', '</small>') ?>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-user" id="nama" name="nama" placeholder="Nama"value="<?= set_value('nama'); ?>">
                                        <?= form_error('nama', '<small class="text-danger pl-3">', '</small>') ?>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-user" id="ttl" name="ttl" placeholder="TTL"value="<?= set_value('ttl'); ?>">
                                        <?= form_error('ttl', '<small class="text-danger pl-3">', '</small>') ?>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-user" id="alamat" name="alamat" placeholder="alamat" value="<?= set_value('alamat'); ?>">
                                        <?= form_error('alamt', '<small class="text-danger pl-3">', '</small>') ?>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-user" id="kelas" name="kelas" placeholder="XI " value="<?= set_value('kelas'); ?>">
                                        <?= form_error('kelas', '<small class="text-danger pl-3">', '</small>') ?>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-user" id="email" name="email" placeholder="3103117355@student.smktel-pwt.sch " value="<?= set_value('email'); ?>">
                                        <?= form_error('email', '<small class="text-danger pl-3">', '</small>') ?>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-user" id="text" name="perusahaan" placeholder="Nama Perusahaan" value="<?= set_value('perusahaan'); ?>">
                                        <?= form_error('Perusahaan', '<small class="text-danger pl-3">', '</small>') ?>

                                    </div>
                                    <!-- <div class="form-group">
                                        <input type="password" class="form-control form-control-user" id="password" name="password" placeholder="*******">
                                        <?= form_error('password', '<small class="text-danger pl-3">', '</small>') ?>
                                    </div> -->
                                    <div class="btn-daftar">
                                        <button type="submit">Daftar</button>
                                    </div>
                                    <hr>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>